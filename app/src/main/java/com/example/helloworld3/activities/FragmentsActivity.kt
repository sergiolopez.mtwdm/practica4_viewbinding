package com.example.helloworld3.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import com.example.helloworld3.R
import androidx.fragment.app.commit
import com.example.helloworld3.databinding.ActivityFragmentsBinding
import com.example.helloworld3.fragments.FirstFragment
import com.example.helloworld3.fragments.SecondFragment

class FragmentsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFragmentsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_fragments)
        binding = ActivityFragmentsBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        val toolbar = findViewById<Toolbar>(R.id.toolbar)
//        setSupportActionBar(toolbar)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_fragment, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.menu_item_fragment1 -> {
                supportFragmentManager.commit {
                    replace(R.id.fragment_container_view, FirstFragment())
                }
            }
            R.id.menu_item_fragment2 -> {
                supportFragmentManager.commit {
                    replace(R.id.fragment_container_view, SecondFragment())
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}