package com.example.helloworld3.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.helloworld3.R
import com.example.helloworld3.databinding.ActivityHelloBinding

class HelloActivity : AppCompatActivity() {

/*    private lateinit var txtName:TextView
    private lateinit var btnBack:Button
    private lateinit var btnBackReturn:Button*/
    private lateinit var binding: ActivityHelloBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_hello)
        binding = ActivityHelloBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*txtName = findViewById(R.id.txt_name)
        btnBack = findViewById(R.id.btn_back)
        btnBackReturn = findViewById(R.id.btn_back_return)*/

        val name = intent.getStringExtra("name")
        var hello = getString(R.string.hello)
//        txtName.text = "$hello $name"
        binding.txtName.text = "$hello $name"

//        btnBack.setOnClickListener {
        binding.btnBack.setOnClickListener {
            finish()
        }
//        btnBackReturn.setOnClickListener {
        binding.btnBackReturn.setOnClickListener {
            var intent = Intent()
            intent.putExtra("valor1", "Test1")
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }
}