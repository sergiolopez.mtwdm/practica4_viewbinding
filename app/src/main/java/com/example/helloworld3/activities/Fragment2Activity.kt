package com.example.helloworld3.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.helloworld3.R
import com.example.helloworld3.databinding.ActivityFragment2Binding

class Fragment2Activity : AppCompatActivity() {

    private lateinit var binding: ActivityFragment2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_fragment2)
        binding = ActivityFragment2Binding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}