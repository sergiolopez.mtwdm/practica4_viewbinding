package com.example.helloworld3.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.helloworld3.R
import com.example.helloworld3.databinding.FragmentMainBinding

class MainFragment : Fragment(R.layout.fragment_main) {

    //    private lateinit var btnFragmentDetail:Button
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
//        return super.onCreateView(inflater, container, savedInstanceState)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        btnFragmentDetail = view.findViewById(R.id.btn_fragment_detail)
        binding.btnFragmentDetail.setOnClickListener {
            var nombre = "Sergio"
            /*findNavController().navigate(R.id.action_mainFragment_to_detailFragment, bundleOf(
                "nombre" to nombre
            ))*/
            val action = MainFragmentDirections.actionMainFragmentToDetailFragment()
            action.nombre = nombre
            findNavController().navigate(action)
        }
    }
}